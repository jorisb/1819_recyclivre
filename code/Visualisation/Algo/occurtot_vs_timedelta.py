# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 10:56:34 2019

@author: longv

A partir du fichier export_commande_nuitducode_etat.csv et du fichier de log de refus,
un autre csv books_entrants_infos.csv a été créée
Nous cherchons à visualiser à quoi ressemble la colonne target (occur_tot)
"""

import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns

annee = 2019

#%% Loading the data books_entrants_infos.csv 

#Use the line below if you want to run the code on the server (Webmin)
#data = pd.read_csv("/home/techfg/1819_recyclivre/code/data/books_entrants_infos.csv")

#Use the lines below if you are running the code on your own computer

path = os.path.join("../..", "data")
data = pd.read_csv(path + "/books_entrants_infos.csv")

#on enlève les livres dont on n'a pas l'année de parution (beaucoup de NaN car là c'es que pour les livres qui apparaissent plus de 50 fois)
books= data.dropna(subset=['AnneeParution'])


#%%
#on enlève les lignes dont les dates d'insertion valent 0
books = books.loc[(books['date_insertion'] != 0)]
books = books.loc[(books['date_insertion'] != '0')]

#on ne prend en compte que les lignes dont les dates de publication sont antérieures à l'année actuelle
books = books.loc[books['AnneeParution'] < annee]

 #on transforme les valeurs des colonnes date_insertion et annee_parution en type "datetime"
books["date_insertion"] = pd.to_datetime(books.date_insertion)
books['AnneeParution'] = books['AnneeParution'].astype(int)



#On considère que tous les livres sont sortis le 01/01/AAAA
books["AnneeParution"] = pd.to_datetime(books.AnneeParution, format='%Y')
books['TimeDeltaYear'] = books["date_insertion"] - books["AnneeParution"]

#avec le dt.days, on transforme le type timedelta en integer. On a alors un résultat en jours
# on divise par 365 pour avoir la différence en année
books['TimeDeltaYear'] = books['TimeDeltaYear'].dt.days / 365

#%%
def occur_vs_date(df, asin, norm=False):
    plt.figure()
    
    books_asin = df.loc[df['asin']== str(asin)]
    books_asin = books_asin.reset_index(drop=True)
    try :
        plt.title("\"" + str(books_asin['Titre'][0]))

    except:
        plt.title("ISBN: " + str(asin))
            
    plt.xlabel('Date_insertion')
    if norm == True:
        plt.ylabel('Nombre de livres reçus selon le mois / Nb total reçus')

        plt.plot(books_asin['date_insertion'], books_asin['occur_tot_normalise'], '*', markersize=3)
        plt.xticks(rotation='vertical')
    else :
        plt.ylabel('Nombre de livres reçus selon le mois')

        plt.plot(books_asin['date_insertion'], books_asin['occur_tot'], '*', markersize=3)
        plt.xticks(rotation='vertical')

occur_vs_date(books,'2742770313')
#%%
        
def multiple_asin_vs_date(df, minimum):
    books_asin = df.loc[df['nb_tot_books']>=minimum]
    print("Nombre d'ASIN différents",len(books_asin['asin'].unique()))
    books_asin = books_asin.reset_index(drop=True)
    print ("Nombre de lignes dans le dataframe sur lequel on travaille", books_asin.shape[0])
    
    sns.lmplot('date_insertion','occur_tot_normalise', hue='asin',data=books_asin, fit_reg=False, scatter_kws={"s": 10})
    
multiple_asin_vs_date(books, 1000)
#%%
def occurtot_vs_TimeDelta(df, asin):
    plt.figure()
    books_asin = df.loc[df['asin']== str(asin)]
    books_asin = books_asin.reset_index(drop=True)
    try :
        plt.title("\"" + str(books_asin['Titre'][0]))

    except:
        plt.title("ISBN: " + str(asin))
            
    plt.xlabel('Durée (en année) entre parution et date_insertion')
    plt.ylabel('Nombre de livres reçus selon le mois')

    plt.plot(books_asin['TimeDeltaYear'], books_asin['occur_tot'], '*', markersize=3)
    plt.xticks(rotation='vertical')

occurtot_vs_TimeDelta(books, '2742770313') #Stieg Larsson, Millenium 3
# occurtot_vs_TimeDelta(books, '207036822X') #1984 Orwell


#%%

def multiple_asin_vs_TimeDelta(df, minimum):
    books_asin = df.loc[df['nb_tot_books']>=minimum]
    books_asin = books_asin.reset_index(drop=True)
    print("Nombre d'ASIN différents",len(books_asin['asin'].unique()))
    print ("Nombre de lignes dans le dataframe sur lequel on travaille", books_asin.shape[0])
    
    lm = sns.lmplot('TimeDeltaYear','occur_tot_normalise', hue='asin',data=books_asin, fit_reg=False, scatter_kws={"s": 10})
    
multiple_asin_vs_TimeDelta(books, 1000)
# ne pas prendre comme minimum la valeur 0 : ça sort un MemoryError


