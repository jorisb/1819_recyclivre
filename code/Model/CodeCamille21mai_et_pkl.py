#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 11:40:30 2019

@author: camilledaveau
"""

# -*- coding: utf-8 -*-

# Import the necessary modules and libraries
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
import os
from sklearn.model_selection import train_test_split

import joblib

# Traitement des données

csv_path = os.path.join("..","data")
incoming_data = pd.read_csv(csv_path + "/books_entrants_infos.csv")

#sur le serveur
#incoming_data = pd.read_csv("/home/techfg/1819_recyclivre/code/data/books_entrants_infos.csv")


incoming_data = incoming_data.dropna(axis=0)

# occur : nombre de livre recu et accepté sur le mois et l'année, peu importe l'etat
# occur_refus : nombre de livre refusé sur le mois
#occur tot : occurences dans le mois
# nb_tot_accepte : nombre total de livre accepté depuis toujours
#nb tot refus : nombre total de livre refusé depuis toujours
# nb tot book : nombre total de libre depuis tjrs
#occur_tot_normalisé= occur tot / nbr total depuis toujours

book_features = ['etat','mois','annee','nb_tot_books','gap']
X = incoming_data[book_features]
y=incoming_data.occur_tot

print(incoming_data.describe())


# Définition du modèle

book_model = DecisionTreeRegressor(random_state=1)

train_X, val_X, train_y, val_y = train_test_split(X, y,test_size=0.33, random_state = 1) 
incoming_model = DecisionTreeRegressor()

incoming_model.fit(train_X, train_y)

val_prediction=incoming_model.predict(val_X)


# Analyse des résultats
print(incoming_model.score(X,y))

#%% Travail sur pkl (Vanessa)
# Save model as pickle file
joblib.dump(incoming_model, "model.pkl")

# Load model from file
classifer = joblib.load("model.pkl")
classifer.predict(val_X)
print(classifer.score(X,y))

ligne00 =val_X.iloc[[0]]
ligne0et1=val_X.iloc[[0,1], :]
ligne0à1bis=val_X.iloc[0:2, :] #ça exclut la 2ème ligne
ligne0à9=val_X.iloc[0:10, :] #ça exlut la 10ème ligne
classifer.predict(ligne00)
classifer.predict(ligne0et1)
predictions = classifer.predict(ligne0à9)


#ça marche !
firstdata = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
test = firstdata.copy()
test0 = test[:10]
test0.loc[test0['asin'] == '222105587X', 'prix_ttc'] += 1
#on peut incrémenter des valeurs, cool

#imaginons j'ai en entrée les 10 dernières lignes
#je veux prédire occur_tot sur ces 10 lignes et exporter ça sous fichier csv
findf = incoming_data.tail(10)
finX = findf[book_features]
predictions = classifer.predict(finX)

submission = pd.DataFrame({'asin':findf['asin'],'occurMMAAAA':predictions})
filename = 'predictionsTEST.csv'
submission.to_csv(filename,index=False)

print('Saved file: ' + filename)
#ça marche
#ya une erreur sur ces 10 lignes
