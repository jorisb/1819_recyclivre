# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 10:25:47 2019

@author: longv
"""

import os
import pandas as pd
import numpy as np

pd.set_option('display.expand_frame_repr', False)

csv_path = os.path.join("data")
data_recyclivre = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
data_recyclivre['date_insertion'] = pd.to_datetime(data_recyclivre.date_insertion)

data_recyclivre['daynumber_insertion'] = data_recyclivre['date_insertion'].dt.dayofweek #0 = Monday, 6 = Sunday
data_recyclivre['daynumber_insertion'].value_counts()


#data_recyclivre['day_of_week_in_number'].hist()
# Commentaire : on voit que peu de livres sont reçues le samedi et le dimanche
# Conclusion : on va faire la distinction jour ouvrés / jour non ouvrés
# et  on ne va pas faire la distinction jour ouvrés / jour ouvrable

data_recyclivre['mois_insertion'] = pd.DatetimeIndex(data_recyclivre['date_insertion']).month
data_recyclivre['jour_insertion'] = pd.DatetimeIndex(data_recyclivre['date_insertion']).day

data_recyclivre['jour_ouvre'] = np.where( (data_recyclivre['daynumber_insertion'] >= 0) & (data_recyclivre['daynumber_insertion'] <= 4 ), 1, 0)
col = ["date_insertion", "mois_insertion", "jour_insertion"]
test = data_recyclivre[col]

test[ (test['mois_insertion'] == 5.0) & (test['jour_insertion'] == 1.0)]

col2 = ["date_insertion", 'daynumber_insertion', 'jour_ouvre']
test2 = data_recyclivre[col2]
    
def infos_jour (isbn):
    csv_path = os.path.join("data")
    data_recyclivre = pd.read_csv(csv_path + "/export_commande_nuitducode_etat.csv")
    
