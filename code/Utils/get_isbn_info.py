# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 14:38:49 2019

@author: camdav
"""

#%% Import
import time
import os
from isbnlib import meta, info, editions, desc, cover
from isbnlib.registry import bibformatters
import pandas as pd


#%% Obtention d'informations sur le livre à partir de l'isbn
def convert_isbn_to_infos(isbn_number, service='openl'):
    """
    Input: ISBN number (String) and service (String)
    Choose your service: 'openl' (OpenLibrary.org) or 'goob' (Google Books)

    Output: Information about the corresponding book (dictionary)
    """
    book = meta(isbn_number, service)
    book["language"] = info(isbn_number)
    book["related_editions"] = editions(isbn_number)
    book["description"] = desc(isbn_number)
    book["cover_info"] = cover(isbn_number)

    return book



#%% Création d'un tableau avec les infos de l'isbn pour tous les asin
def convert_isbn_to_infos_df(df):
    
    
    """
    Input: Dataframe including an 'asin' column
    Choose your service: 'openl' (OpenLibrary.org) or 'goob' (Google Books)

    Output: Information about the corresponding book (dictionary)
    """
    

    title = []
    year = []
    language = []
    for row in df['asin']:
        try:
            book = meta(row, 'goob')
            title.append(book['Title'])
            year.append(book['Year'])
            language.append(book['Language'])
        
        except :
            
            try :
                book = meta(row, 'openl')
                title.append(book['Title'])
                year.append(book['Year'])
                language.append(book['Language'])
            except :
                title.append('NaN')
                year.append(-1000)
                language.append('NaN')
    
    df['Titre'] = title
    df['AnneeParution'] = year
    df['Langue'] = language
    df['AnneeParution'] = pd.to_numeric(df.AnneeParution)
    df = df.loc[df["AnneeParution"] != -1000]
    

    return df


