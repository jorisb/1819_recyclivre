from convert_isbn_infos import convert_isbn_to_infos

def convert_book_to_vector(information_line):
    """
    Input: information about the book (asin, date_vendu, date_insertion, etat, prix_ttc, fdp_fdp
    Output: vector representing the book
    """
    try : 
        #information_split = information_line
        isbn = information_line.asin
        date_vendu = information_line.date_vendu
        date_insertion = information_line.date_insertion
        etat = information_line.etat
        prix_ttc = information_line.prix_ttc
        prix_fdp = information_line.fdp_ttc
    
        book_information = convert_isbn_to_infos(isbn)
        book_vector = {}
        ### Main information
        # Get book's ISBN
        book_vector["isbn13"] = book_information["ISBN-13"]
    
        # Convert state to a 4-boolean vector
        book_vector["state_1"] = 0
        book_vector["state_2"] = 0
        book_vector["state_3"] = 0
        book_vector["state_4"] = 0
        state_str = "state_" + str(etat)
        book_vector[state_str] = 1
    
        book_vector["date_sold"] = date_vendu
        book_vector["date_in"] = date_insertion
        book_vector["price"] = prix_ttc
        book_vector["shipment_cost"] = prix_fdp
    
        # Other information
        book_vector["title"] = book_information["Title"]
        book_vector["authors"] = book_information["Authors"]
        book_vector["publishers"] = book_information["Publisher"]
        book_vector["year"] = book_information["Year"]
        book_vector["language"] = book_information["Language"]
        book_vector["related_editions"] = book_information["related_editions"]
        book_vector["description"] = book_information["description"]
        book_vector["cover_info"] = book_information["cover_info"]


        return book_vector
   
    except : 
        print('error' + isbn)



    book_information = convert_isbn_to_infos(isbn)
    book_vector = []
    
    ### Main information
    # Get book's ISBN
    book_vector["isbn13"] = book_information["ISBN-13"]

    # Convert state to a 4-boolean vector
    book_vector["state_1"] = 0
    book_vector["state_2"] = 0
    book_vector["state_3"] = 0
    book_vector["state_4"] = 0
    state_str = "state_" + str(etat)
    book_vector[state_str] = 1

    book_vector["date_sold"] = date_vendu
    book_vector["date_in"] = date_insertion
    book_vector["price"] = prix_ttc
    book_vector["shipment_cost"] = prix_fdp

    # Other information
    book_vector["title"] = book_information["Title"]
    book_vector["authors"] = book_information["Authors"]
    book_vector["publishers"] = book_information["Publisher"]
    book_vector["year"] = book_information["Year"]
    book_vector["language"] = book_information["Language"]
    book_vector["related_editions"] = book_information["related_editions"]
    book_vector["description"] = book_information["description"]
    book_vector["cover_info"] = book_information["cover_info"]

    return book_vector

