#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 11:22:41 2019

@author: camilledaveau
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from convert_isbn_infos import convert_isbn_to_infos
from convert_book_vector import convert_book_to_vector


dataframe = pd.read_csv('/Users/camilledaveau/Desktop/ncc2019_recyclivre/data/export_commande_nuitducode_etat.csv')
 
dataframe.apply(convert_book_to_vector, axis=1) #il faut revoir comment on obtient les isbn
       
print(dataframe.head())


dataframe = dataframe.loc[dataframe['date_in'] != '0000-00-00'] #on prend que les données ou la date n'est pas 000
dataframe = dataframe.loc[dataframe['date_sold'] != '0000-00-00']
dataframe['date_sold'] = pd.to_datetime(dataframe.date_mel) #conversion en date
dataframe['date_sold'] = pd.to_datetime(dataframe.date_vendu)
dataframe["days"] = (dataframe["date_sold"] - dataframe["date_in"]) #Création d'une nouvelle colonne
dataframe['days'] = pd.to_numeric(dataframe.days)
missing_val_count_by_column = (dataframe.isnull().sum())
print(missing_val_count_by_column[missing_val_count_by_column > 0])
dataframe['days'].value_counts()

corr = dataframe.corr()
sns.heatmap(dataframe.corr(), annot=True, fmt=".2f")
plt.show()