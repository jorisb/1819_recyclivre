# 1819 | RecycLivre x Latitudes

_Projet inno S8_

## Contexte
* L'accompagnement de Latitudes dans le cadre du programme Tech for Good Explorers est opéré selon 3 phases : exploration, prototypage, envol. Pour chacune de ces phases vous trouverez des documents ressources ci-dessous.

* La phase d'exploration permet à toutes les parties prenantes de faire connaissance, de s’approprier les enjeux du projet, et surtout de s’accorder sur les objectifs à atteindre ensemble.
* La phase de prototypage est le cœur du projet, sa réalisation à proprement parler, pour atteindre le livrable fixé en début de projet.
* La phase d'envol est là pour bien conclure l’expérience et se projeter vers la suite.

## Les points d'étapes et les objectifs

* Kick-off
...#1 Finaliser l’intégration de chacun.e au programme Tech for Good Explorers.
...#2 Clarifier les rôles que chacun.e assumera tout au long du programme.
...#3 Créer une dynamique d’équipe, et mettre le projet sur de bons rails.
*Scope review
...#1 Faire le bilan de la phase d’exploration.
...#2 Valider la compréhension du projet, et clarifier son périmètre.
...#3 Se projeter dans la suite du projet, avec un planning clair.
* Mi-projet
...#1 Vérifier que le projet est sur la bonne voie.
...#2 Clarifier – voire modifier au besoin – le livrable final en fonction des avancées et des apprentissages effectués jusque-là.
...#3 Se projeter dans la suite du projet, et anticiper l’après-projet.
* Bilan et ouverture
...#1 Conclure les dernières tâches afin d’arriver à un produit livrable.
...#2 Documenter le travail, afin de faciliter la valorisation du projet, et sa réappropriation par les personnes qui prendront la suite.
...#3 Faire le bilan du projet, ce qui a animé.e, plu, déplu chacun.e, les apprentissages et comment chacun.e se projete au niveau de son engagement.

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès au code source se fait via le dossier "[code](https://gitlab.com/latitudes-exploring-tech-for-good/recyclivre/1819_recyclivre/tree/master/code)".