# 1819 | RecycLivre x Latitudes

_Projet inno S8_

Cette partie contient la documentation détaillée du projet 1819_RecycLivre, avec notamment les livrables :

* relatifs à [CentraleSupélec](https://gitlab.com/latitudes-exploring-tech-for-good/recyclivre/1819_recyclivre/tree/master/documentation/CentraleSup%C3%A9lec)
* faisant partie intégrante de l'[accompagnement Latitudes](https://gitlab.com/latitudes-exploring-tech-for-good/recyclivre/1819_recyclivre/tree/master/documentation/latitudes).

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès au code source se fait via le dossier "[code](https://gitlab.com/latitudes-exploring-tech-for-good/recyclivre/1819_recyclivre/tree/master/code)".
